#! Encoding UTF-8

## 检查中文环境
CN="false"
case $LANG in
  zh_CN*) CN="true";;
esac

[[ -z ${INFO_LOG} ]] \
  && INFO_LOG="~/info_$(date +%F).log"

[[ -z ${ERR_LOG} ]] \
  && ERR_LOG="~/err_$(date +%F).log"

mkdir -p $(dirname ${INFO_LOG})
## 输出打印信息
info(){
  if [[ -z $2 ]] ; then
    TMP_MSG="$1"
  else 
    ${CN} && TMP_MSG="$1" || TMP_MSG="$2"
  fi
  echo "$(date +'%F %H:%M:%S') ${TMP_MSG} "
}

info_log(){
  info "$@" \
    | tee -a ${INFO_LOG}
}

err_log(){
  info "$@" \
    | tee -a ${ERR_LOG}
}

exit_msg(){
  info "$@" \
    | tee -a ${ERR_LOG} \
    | exit 1
}
#! Encoding UTF-8

select_tc_fun(){

  TC_SCRIPT_PATH=${FUNCTION_PATH}/tc

  echo "----------------------------------------------------------------"
  declare -a VAR_LISTS
  if ${CN} ;then
    echo "[Notice] 请选择TC限流方式:"
    VAR_LISTS=("返回首页" "用'hub'限流")
  else
    echo "[Notice] Which tc fun :"
    VAR_LISTS=("Back" "Use'hub'")
  fi
  select VAR in ${VAR_LISTS[@]} ;do
    case ${VAR} in
      ${VAR_LISTS[1]})
        source ${TC_SCRIPT_PATH}/tc_hub.sh;;
      ${VAR_LISTS[0]})
        main;;
      *)
        select_tc_fun;;
    esac
    break
  done
}

select_tc_fun
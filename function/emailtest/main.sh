#! Encoding UTF-8

read_email_var(){
  tmp_msg "请输入发信人完整账号：" "input sender email addr :"
  read -p "${TMP_MSG_RESULT}" SENDER
  echo ""
  tmp_msg "请输入收信人完整账号：" "input reciver email addr :"
  read -p "${TMP_MSG_RESULT}" RECIVER
  echo ""
}

select_download_fun(){

  EMAIL_SCRIPT_PATH=${FUNCTION_PATH}/emailtest

  echo "----------------------------------------------------------------"
  declare -a VAR_LISTS
  if ${CN} ;then
    echo "[Notice] 请选择小功能:"
    VAR_LISTS=("返回首页" "用'telnet'指令发邮件" "用'openssl'指令发邮件(25端口)" "用'openssl'指令发邮件(465端口)")
  else
    echo "[Notice] Which fun :"
    VAR_LISTS=("Back" "Use'telnet'sendmail" "Use'openssl'sendmail_to_25" "Use'openssl'sendmail_to_465")
  fi
  select VAR in ${VAR_LISTS[@]} ;do
    case ${VAR} in
      ${VAR_LISTS[1]})
        read_email_var
        bash ${EMAIL_SCRIPT_PATH}/telnet_mailto.sh ${SENDER} ${RECIVER};;
      ${VAR_LISTS[2]})
        read_email_var
        bash ${EMAIL_SCRIPT_PATH}/openssl_mail_25.sh ${SENDER} ${RECIVER};;
      ${VAR_LISTS[3]})
        read_email_var
        bash ${EMAIL_SCRIPT_PATH}/openssl_mail_465.sh ${SENDER} ${RECIVER};;
      ${VAR_LISTS[0]})
        main;;
      *)
        select_simple_fun;;
    esac
    break
  done
}

select_download_fun